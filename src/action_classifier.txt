digraph Tree {
node [shape=box] ;
0 [label="aheadDistance <= 272.5\ngini = 0.087\nsamples = 933\nvalue = [891, 21, 21]\nclass = noAction"] ;
1 [label="currentLane <= 2.5\ngini = 0.561\nsamples = 44\nvalue = [3, 20, 21]\nclass = moveRight"] ;
0 -> 1 [labeldistance=2.5, labelangle=45, headlabel="True"] ;
3 [label="currentLane <= 1.5\ngini = 0.524\nsamples = 34\nvalue = [3, 10, 21]\nclass = moveRight"] ;
1 -> 3 ;
5 [label="gini = 0.165\nsamples = 11\nvalue = [1, 0, 10]\nclass = moveRight"] ;
3 -> 5 ;
6 [label="gini = 0.575\nsamples = 23\nvalue = [2, 10, 11]\nclass = moveRight"] ;
3 -> 6 ;
4 [label="gini = 0.0\nsamples = 10\nvalue = [0, 10, 0]\nclass = moveLeft"] ;
1 -> 4 ;
2 [label="gini = 0.002\nsamples = 889\nvalue = [888, 1, 0]\nclass = noAction"] ;
0 -> 2 [labeldistance=2.5, labelangle=-45, headlabel="False"] ;
}