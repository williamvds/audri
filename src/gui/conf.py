'''Provides ConfigFrame, the Tkinter frame that allows modifying the visualiser
configuration'''
import tkinter.ttk as ttk
import tkinter as tk

from config import GUIConfig, SimulatorConfig
from .controls import LabeledScale, ToolTip

conf = GUIConfig()
visConf = SimulatorConfig()

class ConfigFrame(tk.Frame):
    '''Frame allowing the configuration of the visualiser

    Contains a number of Tkinter controls allowing the modification of
    attributes in :class:`~config.SimulatorConfig`
    '''

    #: :class:`~config.SimulatorConfig` attributes as keys, associated private
    #: attributes (with a set() method) as attributes. These config values are
    #: all :class:`float`
    _matches = {
        'CarSpeed': 'carSpeed',
        'CarScale': 'carScale',
        'ObstacleSpeed': 'obstSpeed',
        'ObstacleInterval': 'obstFreq'}

    def __init__(self, root, main):
        super().__init__(root,background='white')
        self.grid(padx=50, pady=20)
        self.columnconfigure(0, weight=2, uniform='a')
        self.columnconfigure(1, weight=3, uniform='a')

        self._root = root
        self._main = main
        self._row = 0
        self._col = 0

        tk.Label(self, text=conf.ConfTitle, font=main.fontHeading,background='white') \
            .grid(row=self._row, column=self._col, columnspan=4)
        self._row += 1

        # Expermimental section
        tk.Label(self, text=conf.ConfExperiment, font=main.fontHeading,
            anchor='w',background='white') \
            .grid(row=self._row, column=self._col, columnspan=2, sticky='ew')
        self._row += 1

        self._randomSeed = tk.StringVar()
        self._labelControl(conf.ConfRandomSeed, ttk.Entry,
            {'font': main.fontMonospace, 'textvariable': self._randomSeed})
        self._carSpeed = self._labelControl(conf.ConfCarSpeed, LabeledScale,
            {'font': main.font, 'from_': 0, 'to': 100, 'resolution': 0})
        self._carScale = self._labelControl(conf.ConfCarScale, LabeledScale,
            {'font': main.font, 'from_': 0, 'to': 2})
        self._obstSpeed = self._labelControl(conf.ConfObstSpeed, LabeledScale,
            {'font': main.font, 'from_': 0, 'to': 100, 'resolution': 0})
        self._obstFreq = self._labelControl(conf.ConfObstFreq, LabeledScale,
            {'font': main.font, 'from_': 0, 'to': 100, 'resolution': 2})

        self._offroad = tk.BooleanVar()
        box = ttk.Checkbutton(self, text=conf.ConfOffroad[0],
            variable=self._offroad)
        box.grid(row=self._row, column=self._col, columnspan=2,
            pady=(conf.ConfRowPadding, 0))
        ToolTip(box, conf.ConfOffroad[1])
        self._row += 1

        self._recordFreq = self._labelControl(conf.ConfRecordFreq, LabeledScale,
            {'font': main.font, 'from_': 0, 'to': 100, 'resolution': 2})

        # Other options section
        tk.Label(self, text=conf.ConfAppearance, font=main.fontHeading,
            anchor='w',background='white') \
            .grid(row=self._row, column=self._col, columnspan=2, sticky='ew',
                pady=(conf.ConfRowPadding, 0))
        self._row += 1

        self._fps = self._labelControl(conf.ConfFPS, LabeledScale,
            {'font': main.font, 'from_': 0, 'to': 300, 'resolution': 0})
        self._tickrate = self._labelControl(conf.ConfTickrate, LabeledScale,
            {'font': main.font, 'from_': 1, 'to': 300, 'resolution': 0})

        self._scrollBG = tk.BooleanVar()
        box = ttk.Checkbutton(self, text=conf.ConfBackground[0],
            variable=self._scrollBG)
        box.grid(row=self._row, column=self._col, columnspan=2,
            pady=(conf.ConfRowPadding, 0))
        ToolTip(box, conf.ConfBackground[1])
        self._row += 1

        self._load()

        # Save button
        button = ttk.Button(self, text=conf.ConfSave[0], command=self._save)
        button.grid(row=20, column=0, columnspan=2,
            pady=(conf.ConfRowPadding*2, 0))
        ToolTip(button, conf.ConfSave[1])

    def _labelControl(self, text, control, ctrlOpts={}):
        '''Create a label and a widget, which are placed adjacent on the same
        row.

        :param text: :class:`str` to show in the label
        :param control: :class:`object` class of the control to create
        :param ctrlOpts: :class:`dict` of kwargs to use when constructing the
            control.
        :return: The created :class:`~tkinter.Widget` control
        '''

        lbl = tk.Label(self, text=text[0], anchor='e',background='white')
        lbl.grid(row=self._row, column=self._col, sticky='ew',
            padx=conf.ConfInnerPadding, pady=(conf.ConfRowPadding, 0))
        ToolTip(lbl, text[1])

        ctrl = control(self, **ctrlOpts)
        ctrl.grid(row=self._row, column=self._col+1, sticky='ew',
            padx=conf.ConfInnerPadding,
            pady=(conf.ConfRowPadding, 0))
        ToolTip(ctrl, text[1])
        self._row += 1

        return ctrl

    def _save(self):
        '''Save values into the :class:`~config.SimulatorConfig` from the
        controls
        '''
        print(self._obstFreq.get())
        for opt, ctrl in self._matches.items():
            setattr(visConf, opt, float(getattr(self, '_'+ctrl).get()))

        visConf.RandomSeed = self._randomSeed.get()
        visConf.OffroadAllowed = self._offroad.get()
        visConf.RecordInterval = self._recordFreq.get()
        visConf.FPS = int(self._fps.get())
        visConf.TickRate = int(self._tickrate.get())
        visConf.ScrollBackground = self._scrollBG.get()

        self._main.back()

    def _load(self):
        '''Load values into the controls from the
        :class:`~config.SimulatorConfig`
        '''
        for opt, ctrl in self._matches.items():
            getattr(self, '_'+ctrl).set(getattr(visConf, opt))

        self._randomSeed.set(visConf.RandomSeed)
        self._offroad.set(visConf.OffroadAllowed)
        self._recordFreq.set(visConf.RecordInterval)
        self._fps.set(visConf.FPS)
        self._tickrate.set(visConf.TickRate)
        self._scrollBG.set(visConf.ScrollBackground)
