'''Provides functionality for manipulating training data sets and models'''
import os, csv

from config import SimulatorConfig

conf = SimulatorConfig()

#: (:class:`str`) Path to store data in
DATA_DIR = os.path.dirname(__file__) +'/../data/training'

#: :class:`list` specifying the order of data
DATA_ORDER = ['action', 'aheadDistance', 'currentLane']

#: (:class:`str`) Path to store models in
MODEL_DIR = os.path.dirname(__file__) +'/../data/models'

def modelExists(name):
    ''':param name: (:class:`str`) The name of the model to search for
    :return: :class:`bool` indicating if a model named `name` exists
    '''
    return os.path.isfile(MODEL_DIR+'/'+name)

def dataExists(name):
    ''':param name: (:class:`str`) The name of the data set to search for
    :return: :class:`bool` indicating if a data set named `name` exists
    '''
    return os.path.isfile(DATA_DIR+'/'+name+'.csv')

def loadData(name):
    ''':param name: (:class:`str`) The name of the data set to load
    :return: :class:`dict` containing the training data
    :return: :class:`dict` containing the experimental settings used
    '''
    with open(os.path.join(DATA_DIR, name + '.csv')) as f:
        return [row for row in csv.DictReader(f)]

def saveData(name, data):
    '''Save a data set to a file with a given name.

    Additionally saves a metadata file containing the experimental settings
    used during the training

    :param name: (:class:`str`) The name of the data set to save
    :param data: (:class:`iter` of :class:`dict`) 2-dimensional data structure,
        each list entry is a feature vector.

        Each feature vector is an associative (named) array of features
    '''

    # create DATA_DIR if it doesn't exist
    if not os.path.isdir(DATA_DIR):
        os.makedirs(DATA_DIR)

    # save data
    with open(os.path.join(DATA_DIR, name + '.csv'), 'w+') as f:
        writer = csv.DictWriter(f, fieldnames=DATA_ORDER)
        writer.writeheader()

        for row in data:
            writer.writerow(row)
