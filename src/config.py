'''Configuration classes for all of the components'''

class Singleton(type):
    '''Metaclass that provides singleton behaviour

    :Author: Adam Forsyth <adam@adamforsyth.net>
    :Source: https://stackoverflow.com/a/6798042
    '''

    #: (:class:`dict`) stores the instances of each class
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls) \
                .__call__(*args, **kwargs)
        return cls._instances[cls]

class GUIConfig(metaclass=Singleton):
    '''Configuration used by the :class:`gui` module

    :class:`tuple` attributes store main text at index 0, and tooltip text at
    index 1
    '''

    #: (:class:`int`) Width of the
    #: :class:`visualiser.visualiser.SimulatorVisualiser`
    VisualiserWidth = 500

    #: (:class:`int`) Width of the :class:`~visualiser.visualiser.Panel`
    PanelWidth = 220

    #: (:class:`int`) Height of the main window
    Height = 600

    #: (:class:`str`) Main title shown in the titlebar and the
    #: :class:`~gui.main.MainFrame`
    Title = 'AUDRI Simulator'

    #: (:class:`str`) Subtitle shown in the :class:`~gui.main.MainFrame`
    Subtitle = 'Teaching a computer to drive through example'

    #: (:class:`str`) Text for the button in the :class:`~gui.main.MainFrame`,
    #: linking to the :class:`~gui.sim.Simulator` in compare mode
    CompareText = 'Compete with AUDRI'

    #: (:class:`str`) Text for the button in the :class:`~gui.main.MainFrame`,
    #: linking to the :class:`~gui.sim.Simulator` in training mode
    DataText = 'Gather training data'

    #: (:class:`str`) Text for the button in the :class:`~gui.main.MainFrame`,
    #: linking to the :class:`~gui.conf.ConfigFrame`
    ConfText = 'Configuration'

    #: (:class:`str`) Text for the button in the :class:`~gui.main.MainFrame`
    #: linking to the :class:`~gui.sim.Simulator` in AUDRI mode
    # TODO reference testing frame once implemented
    TestText = 'Test AUDRI'

    #: :class:`list` of :class:`str` mapping simulator modes to a description
    #: of that mode, used by :class:`~visualiser.panel.SimulatorPanel`
    ModeText = ['manual', 'AUDRI', 'compare']

    #: (:class:`str`) Tkinter theme to use
    Theme = 'clam'

    # Fonts: Provide a dict of parameters for each type
    # https://infohost.nmt.edu/tcc/help/pubs/tkinter/web/fonts.html

    #: (:class:`dict`) Data used to create a normal font
    Font = {'family': 'sans-serif', 'size': 10}

    #: (:class:`dict`) Data used to create a bold font
    FontBold = {'family': 'sans-serif', 'size': 10, 'weight': 'bold'}

    #: (:class:`dict`) Data used to create a header font
    FontHeading = {'family': 'sans-serif', 'size': 15, 'weight': 'bold'}

    #: (:class:`dict`) Data used to create a monospaced font
    FontMonospace = {'family': 'monospace', 'size': 10}

    MainButtonXPad = 15 # padding between buttons on same row
    MainButtonYPad = 15 # padding between buttons on different rows

    #: (:class:`int`) Padding between labels and their connected control
    #: in the :class:`~gui.conf.ConfigFrame`
    ConfInnerPadding = 15

    ConfRowPadding = 10 # padding between rows
    TooltipWidth = 250

    #: (:class:`str`) Header text in the :class:`~gui.conf.ConfigFrame`
    ConfTitle = 'Configuration'

    #: (:class:`str`) Header text for the experimental parameters section
    #: in the :class:`~gui.conf.ConfigFrame`
    ConfExperiment = 'Experiment parameters'

    ConfRandomSeed = ('Random seed',
        'The value used to control pseudo-random number generation')
    ConfCarSpeed = ('Car speed',
        '(metres per second)\nThe forward speed of the main car')
    ConfCarScale = ('Car scale',
        'Multiplied against the normal size of the car sprite')
    ConfObstSpeed = ('Obstacle speed',
        '(metres per second)\nThe forward speed of the obstacle vehicles'
        +'\nMust be smaller than car speed')
    ConfObstScale = ('Obstacle scale',
        'Multiplied against the normal size of the obstacle sprites')
    ConfObstFreq = ('Obstacle frequency',
        '(seconds)\nTime between obstacles spawning')
    ConfOffroad = ('Allow the car to drive offroad',
        'If checked, the main car will be able to use the offroad areas to'
        + ' avoid vehicles')
    ConfRecordFreq = ('Snapshot interval',
        'Gap in seconds between recordings of the state of the simulation')

    #: (:class:`str`) Header text for the appearance parameters section
    #: in the :class:`~gui.conf.ConfigFrame`
    ConfAppearance = 'Appearance'
    ConfFPS = ('Frames per second',
        'Frame rate - how many times per second the screen is redrawn'
        +'\nHigher values will likely result in more CPU usage')
    ConfTickrate = ('Tickrate',
        'Tick rate - how many times per second the game logic should run'
        +'\nLower values will impact expermiental accuracy'
        +'\nHigher values will likely result in more CPU usage')
    ConfBackground = ('Scroll background',
        'Whether the background should scroll down the window, as if the car'
        +'\nis travelling down it'
        +'\nDisable if you experience dizziness')

    ConfSave = ('Save',
        'Save configuration and return to the main menu')

    #: Window title of the popup for naming a training set
    #: (:class:`~gui.sim.NameDatasetPopup`)
    SimPopupTitle = 'Name the training set'

    #: Message in (:class:`~gui.sim.NameDatasetPopup`)
    SimPopupText = 'Choose a name for this training set'

    #: Duplicate warning message in (:class:`~gui.sim.NameDatasetPopup`)
    SimPopupDupWarn = 'This name is already in use, overwrite?'

    #: Padding in (:class:`~gui.sim.NameDatasetPopup`)
    SimPopupPad = 10

class SimulatorConfig(metaclass=Singleton):
    '''Configuration used by the :class:`visualiser` class'''

    # Related to Pygame

    #: (:class:`int`) Targeted milliseconds between each simulation tick
    TickRate = 5

    #: (:class:`int`) Targeted frames per second to be drawn
    FPS = 70

    # Appearance

    #: (:class:`bool`) Whether the background should move
    ScrollBackground = True

    #: (:class:`int`) Width of a lane
    LaneWidth = 86

    #: (:class:`int`) Width of a an offroad lane
    OffroadWidth = 120

    #: (:class:`float`) Scale of the car's sprite
    CarScale = 0.35

    #: (:class:`float`) Ratio of pixels to metres, affects the visualisation of
    #: speeds
    PixelMetreRatio = 50

    # Functionality

    #: Random seed used to influence when vehicles spawn
    #: Can be :class:`int`, :class:`str`, and others. See :py:func:`random.seed`
    #: for details
    RandomSeed = "geqJQD6MfJ"

    #: (:class:`float`) Speed of the main car in metres per second
    CarSpeed = 11

    #: | (:class:`float`) Speed of obstacle vehicles in metres per second
    #: | Should be less than :py:attr:`~CarSpeed`
    ObstacleSpeed = 8

    #: (:class:`float`) Seconds between spawning obstacles
    ObstacleInterval = 2

    #: (:class:`float`) Seconds between each snapshot
    RecordInterval = .2

    #: (:class:`bool`) Whether the car can go offroad
    OffroadAllowed = False
