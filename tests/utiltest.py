import unittest
import visualiser
from visualiser import util
from visualiser.util import Actions
from visualiser.util import Vector
from visualiser.util import Pos


class TestUtilMethods (unittest.TestCase):

    def test_loadSprite(self):
        '''check that correct image is returned'''
        #TODO

#The following tests are for the Vector class

    def test_add(self):
        v = Vector()
        val = Vector()
        val.x = 1
        val.y = 1
        v.x = 0
        v.y = 0
        v = v.__add__(val)
        self.assertEqual(v.x, 1, 'x value is incorrect')
        self.assertEqual(v.y, 1, 'y value is incorrect')

    def test_sub(self):
        v = Vector()
        val = Vector()
        val.x = 1
        val.y = 1
        v.x = 2
        v.y = 2
        v = v.__sub__(val)
        self.assertEqual(v.x, 1, 'x value is correct')
        self.assertEqual(v.y, 1, 'y value is correct')

    def test_mul(self):
        v = Vector()
        val = Vector()
        val.x = 2
        val.y = 2
        v.x = 2
        v.y = 2
        v = v.__mul__(val)
        self.assertEqual(v.x, 4, 'x value is correct')
        self.assertEqual(v.y, 4, 'y value is correct')

    def test_div(self):
        v = Vector()
        val = Vector()
        val.x = 2
        val.y = 2
        v.x = 4
        v.y = 4
        v = v.__div__(val)
        self.assertEqual(v.x, 2, 'x value is correct')
        self.assertEqual(v.y, 2, 'y value is correct')

    def test_repr(self):
        v = Vector()
        v.x = 4
        v.y = 4
        self.assertEqual(v.__repr__(),'Vector'+ str((v.x, v.y)), 'strings are same')


#the following tests are for the Pos class

    def test_x(self):
        '''check x value is returned'''
        v = Vector(0,0)
        self.assertEqual(v.x, 0, 'x incorrect')

    def test_x_setter(self):
        '''check that x is set to given value 'val' '''
        v = Vector()
        v._x = 5
        self.assertEqual(v._x, 5, 'x incorrect')

    def test_y(self):
        '''check that y value is returned'''
        v = Vector(0,0)
        self.assertEqual(v.y, 0, 'x incorrect')

    def test_y_setter(self):
        '''check that y is set to given value 'val' '''
        v = Vector()
        v._y = 5
        self.assertEqual(v._y, 5, 'y incorrect')

