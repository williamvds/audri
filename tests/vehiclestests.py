'''Tests for the Vehicle class'''
# pylint: disable=protected-access,wrong-import-position,invalid-name
import unittest, sys
import pygame

sys.path.append('src')
from visualiser.vehicles import Vehicle, Obstacle
from visualiser.util import Pos, Vector
from config import SimulatorConfig

from .mocking import Surface, Visualiser

class TestVehiclesMethods(unittest.TestCase):
    '''Tests for the Vehicle class'''

    def test_pos(self):
        '''Check correct position returned from Vehicle.pos'''
        v = Vehicle(None)
        v._pos = Pos(v, 24, 42)
        self.assertEqual(v.pos.x, 24, 'x value is different')
        self.assertEqual(v.pos.y, 42, 'y value is different')

    def test_pos_setter(self):
        '''Check correct position is set on Vehicle.pos'''
        v = Vehicle(None)
        v.rect = pygame.Rect(0, 0, 10, 10)
        v.pos = Pos(v, 24, 42)
        self.assertEqual(v.pos.x, 24, 'x value is different')
        self.assertEqual(v.pos.y, 42, 'y value is different')

    def test_speed(self):
        '''Check correct speed is returned from Vehicle.speed'''
        v = Vehicle(None)
        v.speed = 10
        self.assertEqual(v._speed, 10, 'different value returned')

    def test_sprite(self):
        '''Test Vehicle.sprite properly creates and returns sprites'''
        # When _image set
        v = Vehicle(None)
        obj = list()
        v._image = obj
        self.assertIs(v.sprite, obj, 'Vehicle._image not returned when set')

        # When _image not set
        v._image = None
        ret = v.sprite
        self.assertIsInstance(ret, pygame.Surface,
            'a newly created Pygame surface was not returned')
        self.assertIsInstance(v._image, pygame.Surface,
            'a newly created Pygame surface was not stored in Vehicle._image')
        self.assertIs(v._image, ret,
            'Vehicle._image is not the same as the returned value')

    def test_lane(self):
        '''Check correct lane is returned from Vehicle.lane'''
        v = Vehicle(None)
        v._lane = 2
        self.assertEqual(v.lane, 2, 'an incorrect value was returned')

    def test_lane_setter(self):
        '''Check lane is set correctly using Vehicle.lane
        Should not allow values outside range [0,4]
        The Vehicle's position should be updated'''
        v = Vehicle(None)
        v.rect = pygame.Rect(0, 0, 10, 10)
        c = SimulatorConfig()

        v.lane = 2
        self.assertEqual(v._lane, 2, 'valid value not accepted')
        v.lane = -1
        self.assertEqual(v._lane, 2, 'invalid value accepted')

        # Offroad allowed
        c.OffroadAllowed = True
        v._lane = 2
        v.lane = 0
        self.assertEqual(v._lane, 0,
            'offroad lane not accepted when it should be')
        v.lane = 4
        self.assertEqual(v._lane, 4,
            'offroad lane not accepted when it should be')

        # Offroad not allowed
        c.OffroadAllowed = False
        v._lane = 2
        v.lane = 0
        self.assertEqual(v._lane, 2,
            'offroad lane accepted when it should not be')
        v.lane = 4
        self.assertEqual(v._lane, 2,
            'offroad lane accepted when it should not be')

    def test_colliding(self):
        '''Test whether vehicle collision is properly detected by
        Vehicle.colliding'''
        v1 = Vehicle(None)
        v1.rect = pygame.Rect(0, 0, 10, 10)
        v2 = Vehicle(None)
        v2.rect = pygame.Rect(5, 5, 10, 10)
        self.assertTrue(v1.colliding(v2), 'collision not properly detected')

    def test_draw(self):
        '''Check that the vehicle is drawn'''
        v = Vehicle(None)
        surf = Surface()
        v.draw(surf)
        self.assertTrue(surf.hasDrawn, 'no draw call')

    def test_tick(self):
        '''Check that Vehicle is moved on tick'''
        v = Vehicle(None)
        v.rect = pygame.Rect(0, 0, 10, 10)
        v.velocity = Vector(1, 1)
        v.tick(1)
        self.assertEqual(v.pos.x, 1, 'x position incorrect')
        self.assertEqual(v.pos.y, 1, 'y position incorrect')

    # The following tests are for the Obstacle class

    def test_hasCollided(self):
        '''Check Vehicle.hasCollided return value and sets Vehicle._collided'''
        game = Visualiser()
        game.car.rect = pygame.Rect(0, 0, 10, 10)
        game.car.pos = Pos(game.car, 0, 0)
        v = Obstacle(game)
        # TODO initialise an Obstacle without Pygame
        v.rect = pygame.Rect(0, 0, 10, 10)
        self.assertTrue(v.hasCollided(),
            'collision with car not properly detected')
        self.assertTrue(v._collided, 'collision not saved')
        self.assertFalse(v.hasCollided(),
            'hasCollided returned true more than once')

    def test_obstacle_speed(self):
        '''check that the speed mthod in this class returns the correct speed'''
        #TODO

    def test_obstacle_speed_setter(self):
        '''test that the vehicles speed is updated as expected'''
        #TODO

    def test_obstacle_tick(self):
        '''check that false is returned if vehicle is off screen'''
        #TODO

    # The following tests are for the Car class

    def test_car_tick(self):
        '''Method currently does nothing.
        Update descirption when this method is decided'''
        #TODO
