# V1.0 Release Review
### Expectations 
The expectations for V1.0 release were that the product would have minimal 
functionality to allow the system to be demonstrated and used by any person. 
This included a functioning simulator and audri model which would be able to 
train on recorded data and return an action.    
The simulator should have a basic display and controls in place to allow a user 
to use the system. The simultor should also have functionality for collecting 
data which can be used to train the system.    
The audri model should consist of an algorithm which is able to use training 
data to return an action when the simulator is run in 'audri mode'. 


### Issues
The planned release for V1.0 was delayed as reflected in timeline.md.   
The original specifications required that the algorithm used for audri would be
a neural network. This was later changed to a decision tree due to functionality
issues. 
The decision tree in V1.0 returns an action as expected for this release, 
however further work is needed for optimal performance. 

### The Deliverable
V1.0 of audri provides basic functionality as described in expectations. However 
the performance is poor. Audri occasionally moves into other vehicles resulting
in a collision. 

### Next Steps 
##### The Simulator 
The top priority for the simulator is to provide functionality for duel screens. 
The aim of this is to allow users to play along side audri to compair their 
performance. 

##### Audri model 
The top priority for the audri model is to improve performance and reduce the 
number of collisions that audri encounters. This may include altering the 
training data/parameters used by the decision tree. 
