### docs - Documents
Any documents related to the project, and extra resources that would be helpful

- `design`: Documents relating to the design of the project

- `TrainingDataSpec.md`: Specifications for how training data should be stored
- `AUDRITensorflowPOC.md`: Details of how the machine learning side of AUDRI
should be implemented
- `repository.md`: Repository organisation, standards, and guidelines
- `resources.md`: Useful resources for the development of the project
- `kanban.md`: Kanban usage guidelines
- `code.md`: Code style guidelines
- `ChangeOfSpec.md`: Report on the change of specification from neural network 
 to decision tree. 
- `V1releaseReview.md`: Report on the first release and next steps. 

Users stories, specifications, meetings and proof of concepts can be found in 
issues under the 'documentation' label. 