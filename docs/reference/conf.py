#!/usr/bin/env python3
import sys, os

project = 'AUDRI'
author = 'Pierce James Morris, Amber Louise Elliot, Cameron Hubbard' \
    +', William Vigolo da Silva, Hao Sun'
desc = 'Learning to drive by imitating a human'
copyright = '2018 Team TODO'

version = ''
release = ''
language = 'en'

sys.path.insert(0, os.path.abspath('../../src/'))

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.todo',
    'sphinx.ext.intersphinx',
    'sphinx.ext.viewcode',
]

templates_path = ['templates']
html_static_path = ['static']
exclude_patterns = ['build', 'Thumbs.db', '.DS_Store']

source_suffix = '.rst'
master_doc = 'index'

# link to external documentation
intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None),
    'pygame': ('https://www.pygame.org/docs', None),
}

# show private members
autodoc_default_flags = [
    'private-members',
]

# provide project name in rst files
rst_prolog = '.. |project| replace:: {0}'.format(project)

pygments_style = 'sphinx'
html_theme = 'alabaster'
html_theme_options = {
    'show_related': True,
}
html_sidebars = {
    '**': [
        'globaltoc.html',
        'relations.html',
        'searchbox.html',
    ]
}
htmlhelp_basename = 'srcdoc'

latex_elements = {
    'papersize': 'letterpaper',
    'pointsize': '10pt',
    'preamble': '',
    'figure_align': 'htbp',
}
latex_documents = [
    (master_doc, 'src.tex', project+' Documentation',
     'Author', 'manual'),
]

man_pages = [
    (master_doc, 'src', project+' Documentation', [author], 1)
]

texinfo_documents = [
    (master_doc, 'src', project+' Documentation', author, 'src', desc,
    'Miscellaneous'),
]

epub_title = project
epub_author = author
epub_publisher = author
epub_copyright = copyright
epub_exclude_files = ['search.html']
