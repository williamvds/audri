### Details
<!--
  Background information about the task
  Extra useful information such as resources
  This section should be updated with feature branch name if one is being used
-->

### Deliverables
<!--
  Any specific documents or files that should be produced as a part of this task
  - Use bullet points
  Don't include additions to existing code
  Remove this section if unused
-->

### Requirements
<!--
  Things that must be done for this tasks to be considered completed
  - Bullet points are ideal
  - [ ] Use checkboxes for subtasks
    - they can be ticked off as they are completed
    - Add a list at another indent level to give more details for a subtask
-->

### Potential additions
<!--
  Additional subtasks that are unnecessary, but could extend the task
  Use checkboxes and bullet points as in the Requirements section
  Remove this section if unused
-->

### Assigned to
<!--
  Only one member can be assigned using /assign
  In cases where there are multiple members working on a single task, list them using @username
  so they are shown as participants
  If no one is yet assigned to this task, leave this section blank
  If someone will be working alone on this task, /assign them and remove this section
-->
